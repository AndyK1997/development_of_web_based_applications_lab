<?php


abstract class Framework
{
    private $dbHost;
    private $dbPassword;
    private $dbUser;
    private $dbname;
    protected $dataBase;

    protected function __construct()
    {
        $this->dbHost="mariadb";
        $this->dbUser="public";
        $this->dbPassword="public";
        $this->dbname="MyDatabase";
    }

    public function __destruct()
    {
       // $this->dataBase->close();
    }

    private function setDbConnection(){
        $this->dataBase=new MySQLi($this->dbHost,$this->dbUser,$this->dbPassword,$this->dbname);
        if(mysqli_connect_errno()){
            throw new Exception(mysqli_connect_error());
        }
        if(!$this->dataBase->set_charset("utf8")){
            throw new Exception($this->dataBase->error);
        }
    }

    protected function ConnectDatabase(){
        try{
            $this->setDbConnection();
        }catch (Exception $e){
            echo $e->getMessage();
        }
    }

    protected function getDatabaseData($sqlString){
        $Array =array();
        $this->ConnectDatabase();
        try{
            $recordset=$this->dataBase->query($sqlString);
            if(!$recordset){
                throw new Exception("Query failed".$this->dataBase->error);
            }else{
                for($i=0; $dbElement=$recordset->fetch_assoc();$i++){
                    $Array[$i]=$dbElement;
                }
                $recordset->free();
            }
        }catch (Exception $e){
            echo $e->getMessage();
        }

        return $Array;
    }


    protected function generateHtmlHead($TabDescription){
        echo<<<EOT
         <head>
            <meta charset="utf-8">
            <meta name="keywords" content="googleSuchwort1, googleSuchwort2, etc"> 
            <meta name="description" content="Beschreibung des webseiten Inhalts">
            <meta name="author" content="Andreas Kirchner">
            <link rel='stylesheet' type='text/css' href='style.css'><!--Einbindung der CSS Datei-->
            <script src='script.js'></script><!--Einbindung der JavaScript Datei-->
            <title>$TabDescription</title>
            </head>
EOT;

    }

    protected function generateHtmlNavElemente($Array){
        echo<<<EOT
        <nav>
EOT;
        foreach ($Array as $element){
            $link=$element["link"];
            $description=$element["description"];
            echo<<<EOT
            <a href="$link">$description</a>
EOT;
        }
        echo"</nav>";
    }

    protected  function generateHtmlFoot($string){
        echo<<<EOT
        <footer>
        <p>$string</p>
        </footer>
        </body>
        </html>
EOT;

    }


}