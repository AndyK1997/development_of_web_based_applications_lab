<?php

require_once 'Framework.php';
class Main extends Framework
{
    private $viewData;
    private $NavMenue;

    protected function __construct()
    {
        parent::__construct();
        $this->NavMenue=array();
        $this->NavMenue[0]["link"]="Main.php";
        $this->NavMenue[0]["description"]="Start";
        $this->NavMenue[1]["link"]="http://www.google.de";
        $this->NavMenue[1]["description"]="Zeugnisse";
        $this->NavMenue[2]["link"]="http://www.google.de";
        $this->NavMenue[2]["description"]="Fähigkeiten";
        $this->NavMenue[3]["link"]="http://www.google.de";
        $this->NavMenue[3]["description"]="Bilder";
        $this->NavMenue[4]["link"]="http://www.google.de";
        $this->NavMenue[4]["description"]="Kontakt";
    }

    public function __destruct()
    {
        parent::__destruct();
    }

    protected function getViewData(){
        $sql="SELECT * FROM test Order by id";
        $this->viewData=$this->getDatabaseData($sql);
    }

    protected function generateView(){
        //$this->getViewData();
        parent::generateHtmlHead("Andreas Homepage");
        echo <<<EOT
        <header><h1>Herzlich Willkommen auf meiner Seite</h1><hr></header>
EOT;
        parent::generateHtmlNavElemente($this->NavMenue);
        echo<<<EOT
        <section class="Information">
        <h3>Über mich</h3>
        <p>Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua. At vero eos et accusam et justo duo dolores et ea rebum. Stet clita kasd gubergren, no sea takimata sanctus est Lorem ipsum dolor sit amet. Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua. At vero eos et accusam et justo duo dolores et ea rebum. Stet clita kasd gubergren, no sea takimata sanctus est Lorem ipsum dolor sit amet.</p>
        </section>
        <section class="personalData">
        <img src="picture/profilePicture.jpg" width="200" height="250" alt="" title="Profilbild von Andreas Kirchner">
        <h2>Andreas Kirchner</h2>
</section>
EOT;

        parent::generateHtmlFoot("Hallo World");

    }

    protected function processReceivedData(){

    }

    public static function main()
    {
        try {
            $page = new Main();
            $page->processReceivedData();
            $page->generateView();
        }
        catch (Exception $e) {
            header("Content-type: text/plain; charset=UTF-8");
            echo $e->getMessage();
        }
    }
}

Main::main();