<?php header ("Content-type: text/html"); ?>
<!DOCTYPE html>

<html lang="de" xmlns="http://www.w3.org/1999/xhtml">
    <head>
        <meta charset="utf-8"/>
        <meta name="keywords" content="Essen, Pizza, Service"> 
        <meta name="description" content="HTML lernen mit Website für Praktikum 0">
        <meta name="author" content="Stefan &amp; Andreas">
        <meta http-equiv="refresh" content="120">

        <title>Pizzaservice Binary</title>
    </head>

    <body>
        <h1>Pizzaservice Binary</h1>
        <p>Bestellen Sie bei uns die beste Pizza im Landkreis!</p>
        <hr>
        <h2>Unsere Pizzen:</h2>
        <form action="https://echo.fbi.h-da.de/" method="POST">
            <article>
                <h3>Pizza Margherita</h3>
                <p>Belegt mit: Tomatensoße, Gauda und verfeinert mit Basilikum.</p>
                <img src="Pizza_Margherita.jpg" alt="Pizza Margherita" height="400">
                <h4>6,50€</h4>
                <hr>
            </article>

            <article>
                <h3>Pizza Salami</h3>
                <p>Belegt mit: Salami, Tomatensoße, Gauda und verfeeinert mit Petersilie.</p>
                <img src="Pizza_Salami.jpg" alt="Pizza Salami" height="400">
                <h4>8,50€</h4>
                <hr>
            </article>

            <article>
                <h3>Pizza Spezial</h3>
                <p>Belegt mit: Salami, Schinken, Pilzen, Tomatensoße, Tomaten Scheiben, Gauda</p>
                <img src="Pizza_Special.jpg" alt="Pizza Spezial" height="400">
                <h4>10,50€</h4>
                <hr>
            </article>

            <article>
                <h3>Warenkorb:</h3>
                <select name="Warenkorb[]" size="6" multiple>
                    <option>Salami</option>
                    <option>Salami</option>
                    <option>Margherita</option>
                    <option>Spezial</option>
                    <option>Spezial</option>
                </select>
                <input type="submit" name="Auswahlloeschen" value="Auswahl löschen"/>
                <input type="submit" name="AlleLoeschen" value="Warenkorb leeren"/>
                <h3>Platzhalter Gesamtpreis €</h3> 
                <input type="text" name="Adresse" value="Ihre Adresse"/><br/>
                <input type="submit" name="Bestellen" value="Bestellen"/>
            </article>
        </form>
    </body>
</html>
