<?php header ("Content-type: text/html");

$MENUE_BAR = array("Bestellung", "Fahrer", "Baecker", "Kunde");

?>

<!DOCTYPE html>

<html lang="en" xmlns="http://www.w3.org/1999/xhtml">
<head>
    <meta charset="utf-8" />
    <meta name="keywords" content="Pizza, Bestellen, Lieferservice, Essen, Darmstadt, Offenbach"/>
    <meta name="description" content="Pizza Lieferservice H-da 0.Praktikum" />
    <meta name="author" content="Kirchner, Jaeger"/>
    <meta http-equiv="refresh" content="60"/>
    <meta name="robots" content="index, follow"/> <!--alle (auch folgenden) Seiten werden indiziert-->
    <meta name="revisit-after" content="3 month" /> <!--nach 3 Monaten die Seite neu indizieren-->

    <title>PizzaLieferservice Luigi</title> <!--Titel für Tab-->
</head> 
    <body>
        <h1>Pizza Lieferservice Luigi</h1>
        <hr>
        <table>
            <tr>
                <td>
                    <form action="bestellung.php" method="POST">
                    <input type="submit" name=$MENUE_BAR[0] value="Bestellen"  />
                    </form>
                </td>
                <td>
                    <form action="Pizzabaecker.php">
                        <input type="submit" name= $MENUE_BAR[2] value="Baecker"/>
                    </form>
                </td>
                <td>
                    <form action="Fahrer.php">
                        <input type="submit" name= $MENUE_BAR[1] value="Fahrer"/>
                    </form>
                </td>
                <td>
                    <form action="Kunde.php">
                        <input type="submit" name= $MENUE_BAR[3] value="Kunde"/>
                    </form>
                </td>
            </tr>
        </table>
    </body>
</html>