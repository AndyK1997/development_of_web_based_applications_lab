<?php header ("Content-type: text/html"); ?>
<!DOCTYPE html>

<html lang="de" xmlns="http://www.w3.org/1999/xhtml">
    <head>
        <meta charset="utf-8"/>
        <meta name="keywords" content="Essen, Pizza, Service"> 
        <meta name="description" content="HTML lernen mit Website für Praktikum 0">
        <meta name="author" content="Stefan &amp; Andreas">
        <meta http-equiv="refresh" content="360">

        <title>Kundenseite Pizzaservice Binary</title>
    </head>

    <body>
        <h1>Pizzaservice Binary</h1>
        <hr>
        <h2>Informationsseite für den Kunden- <br>Lieferstatus</h2>
        <form action="https://echo.fbi.h-da.de/" method="POST">
            <table>
                <tr>
                    <td>Salami: </td>
                    <td>status</td>
                </tr>

                <tr>
                    <td>Margherita: </td>
                    <td>status</td>
                </tr>

                <tr>
                    <td>Spezial: </td>
                    <td>status</td>
                </tr>
            </table>

            <section><hr><input type="submit" name="zurückZuBestellseite" value="Neue Bestellung"/></section>
        </form>
    </body>
</html>