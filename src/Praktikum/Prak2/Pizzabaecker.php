<?php

require_once './Page.php';
class Pizzabaecker extends Page
{
    // to do: declare reference variables for members
    // representing substructures/blocks
    protected $BestellDaten =null;
    /**
     * Instantiates members (to be defined above).
     * Calls the constructor of the parent i.e. page class.
     * So the database connection is established.
     *
     * @return none
     */
    protected function __construct()
    {
        parent::__construct();
        // to do: instantiate members representing substructures/blocks
    }

    /**
     * Cleans up what ever is needed.
     * Calls the destructor of the parent i.e. page class.
     * So the database connection is closed.
     *
     * @return none
     */
    public function __destruct()
    {
        parent::__destruct();
    }

    /**
     * Fetch all data that is necessary for later output.
     * Data is stored in an easily accessible way e.g. as associative array.
     *
     * @return none
     */
    protected function getViewData()
    {
        // to do: fetch data for this view from the database
        $sql="SELECT o.id, o.f_order_id, o.status, a.name FROM ordered_articles o JOIN article a ON a.id=o.f_article_id ";
        $res=$this->_database->query($sql);
        if($res->num_rows>0){
            $cnt=0;
            while($index=$res->fetch_assoc()){

                $this->BestellDaten[$cnt]=$index;
                $cnt++;
            }
        }else{
            echo "Database access error".$this->_database->error;
        }
    }

    /**
     * First the necessary data is fetched and then the HTML is
     * assembled for output. i.e. the header is generated, the content
     * of the page ("view") is inserted and -if avaialable- the content of
     * all views contained is generated.
     * Finally the footer is added.
     *
     * @return none
     */
    protected function generateView()
    {
        $this->getViewData();
        $this->generatePageHeader("Pizzabaecker");
        // to do: call generateView() for all members
        // to do: output view of this page
        echo "
        <head>
        <meta charset=\"utf-8\"/>
        <meta name=\"keywords\" content=\"Essen, Pizza, Service\"> 
        <meta name=\"description\" content=\Pizza service status seite\">
        <meta name=\"author\" content=\"Stefan &amp; Andreas\">
        <meta http-equiv=\"refresh\" content=\"5\">
        <title>Pizzabaereckerseite</title>
        </head>
        <body>
        <h1>Pizzaservice Binary</h1>
        <p>Bestellen Sie bei uns die beste Pizza im Landkreis!</p>
        <hr>
        <h2>Bestellungen:</h2>
        ";
        echo"<ul>";
        foreach ($this->BestellDaten as $index){
            $id=$index["id"];
            $customerId=$index["f_order_id"];
            $Pizza=$index["name"];
            echo"<form action='Pizzabaecker.php' method='POST'>";
            echo"<li><p>BestellNr: ".$id." KundenNr: ".$customerId." Pizza: ".$Pizza." ";
            switch($index["status"]) {
                case 0:
echo <<<EOT
                Bestellt: <input type="radio" name="$id" value="0" checked>
                Backt: <input type="radio" name="$id" value="1">
                fertig gebacken: <input type="radio" name="$id" value="2">
                im Fahrzeug: <input type="radio" name="$id" value="3">
                <input type="submit" name="aktualisieren" value="Backt"></p></li></form>
EOT;
                    break;
                case 1:
echo <<<EOT
                Bestellt: <input type="radio" name="$id" value="0" >
                Backt: <input type="radio" name="$id" value="1" checked>
                fertig gebacken: <input type="radio" name="$id" value="2">
                im Fahrzeug: <input type="radio" name="$id" value="3">
                <input type="submit" name="aktualisieren" value="fertig gebacken"></p></li></form>
EOT;
                    break;
                case 2:
echo <<<EOT
                Bestellt: <input type="radio" name="$id" value="0" >
                Backt: <input type="radio" name="$id" value="1">
                fertig gebacken: <input type="radio" name="$id" value="2" checked>
                im Fahrzeug: <input type="radio" name="$id" value="3"></p></li></form>                  

EOT;
                    break;
                case 3:
echo <<<EOT
                Bestellt: <input type="radio" name="$id" value="0" >
                Backt: <input type="radio" name="$id" value="1">
                fertig gebacken: <input type="radio" name="$id" value="2">
                im Fahrzeug: <input type="radio" name="$id" value="3" checked></p></li></form>
EOT;
                    break;
                case 4:
                    //If Order delivered show nothing
                    break;
                default: echo"Error Invalid status number";
            }

        }
        echo"</ul>";
        //echo"<input type='submit' name='aktualisieren' value='Aktualisieren'>";
        $this->generatePageFooter();
    }

    /**
     * Processes the data that comes via GET or POST i.e. CGI.
     * If this page is supposed to do something with submitted
     * data do it here.
     * If the page contains blocks, delegate processing of the
     * respective subsets of data to them.
     *
     * @return none
     */
    protected function processReceivedData()
    {
        parent::processReceivedData();
        // to do: call processReceivedData() for all
        var_dump($_POST);
        if(isset($_POST)) {
            foreach ($_POST as $key => $value){
                $sql ="UPDATE ordered_articles SET status='$value'+1 WHERE id=$key";
                $this->_database->query($sql);
            }
        }
    }

    /**
     * This main-function has the only purpose to create an instance
     * of the class and to get all the things going.
     * I.e. the operations of the class are called to produce
     * the output of the HTML-file.
     * The name "main" is no keyword for php. It is just used to
     * indicate that function as the central starting point.
     * To make it simpler this is a static function. That is you can simply
     * call it without first creating an instance of the class.
     *
     * @return none
     */
    public static function main()
    {
        try {
            $page = new Pizzabaecker();
            $page->processReceivedData();
            $page->generateView();
        }
        catch (Exception $e) {
            header("Content-type: text/plain; charset=UTF-8");
            echo $e->getMessage();
        }
    }
}

// This call is starting the creation of the page.
// That is input is processed and output is created.
Pizzabaecker::main();
