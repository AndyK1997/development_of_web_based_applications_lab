<?php

require_once './Page.php';
class Fahrer extends Page
{
    // to do: declare reference variables for members
    // representing substructures/blocks
    protected $allOrders=null;       //contains all Orders
    protected $allCutomer=null;      //contains all Customer/orderings

    /**
     * Instantiates members (to be defined above).
     * Calls the constructor of the parent i.e. page class.
     * So the database connection is established.
     *
     * @return none
     */
    protected function __construct()
    {
        parent::__construct();
        // to do: instantiate members representing substructures/blocks
    }

    /**
     * Cleans up what ever is needed.
     * Calls the destructor of the parent i.e. page class.
     * So the database connection is closed.
     *
     * @return none
     */
    public function __destruct()
    {
        parent::__destruct();
    }

    /**
     * Fetch all data that is necessary for later output.
     * Data is stored in an easily accessible way e.g. as associative array.
     *
     * @return none
     */
    protected function getViewData()
    {
        // to do: fetch data for this view from the database
        $sql="SELECT o.id, o.f_article_id, o.f_order_id, o.status, a.name, a.price FROM ordered_articles o JOIN article a ON a.id=o.f_article_id "; //create a join table with article and orders
        $this->allOrders=$this->_database->query($sql);
        $this->allCutomer=$this->_database->query("SELECT * FROM ordering");

    }

    /**
     * First the necessary data is fetched and then the HTML is
     * assembled for output. i.e. the header is generated, the content
     * of the page ("view") is inserted and -if avaialable- the content of
     * all views contained is generated.
     * Finally the footer is added.
     *
     * @return none
     */
    protected function generateView()
    {
        $this->getViewData();
        $this->generatePageHeader('to do: change headline');
        // to do: call generateView() for all members
        // to do: output view of this page
        echo"
         <head>
        <meta charset=\"utf-8\"/>
        <meta name=\"keywords\" content=\"Essen, Pizza, Service\"> 
        <meta name=\"description\" content=\Pizza service status seite\">
        <meta name=\"author\" content=\"Stefan &amp; Andreas\">
        <meta http-equiv=\"refresh\" content=\"5\">
        <title>Fahrer Seite</title>
        </head>
        <body>
        <h1>Pizzaservice Binary</h1>
        <p>Bestellen Sie bei uns die beste Pizza im Landkreis!</p>
        <hr>
        <h2>Fertig zur Auslieferung:</h2>";

    foreach ($this->allCutomer as $Customer){
        $expectedOrderSIZE=0;
        $ReadyforDelivering=0;
        $currentInDelivering=0;
        $ButtonText=null;
        $billingPrice=0;
        $allPizzas=array();
        $Pizzacnt=0;
        foreach ($this->allOrders as $Order){       //count the expected order size and prefeching the order data
            if($Order["f_order_id"]==$Customer["id"]){
                $expectedOrderSIZE++;
                $allPizzas[$Pizzacnt]=$Order["name"];
                $billingPrice+=$Order["price"];
                $Pizzacnt++;
            }
        }

        foreach ($this->allOrders as $order){           //detect if order in the car or ready to pickup
            if($order["f_order_id"]==$Customer["id"] and $order["status"]==2){
                $ReadyforDelivering++;
            }
            elseif($order["f_order_id"]==$Customer["id"] and $order["status"]==3){
                $currentInDelivering++;
            }
        }

        if($ReadyforDelivering==$expectedOrderSIZE){        //if ready to pickup generate website content
            echo "<form action='Fahrer.php' method='POST'>";
            echo "<p>KNR: ".$Customer['id']." Adresse:".$Customer['address']." Enthält:";
            for($x=0; $x<count($allPizzas); $x++){
                echo $allPizzas[$x].", ";
            }
            echo "Preis:".$billingPrice."</p>";
            $ButtonText="Im Fahrzeug";
            echo "<input type='submit' name='$Customer[id]' value='$ButtonText'></form>";

        }elseif ($currentInDelivering==$expectedOrderSIZE){ //if in delivery generate website content
            echo "<form action='Fahrer.php' method='POST'>";
            echo "<p>KNR: ".$Customer['id']." Adresse:".$Customer['address']." Enthält:";
            for($x=0; $x<count($allPizzas); $x++){
                echo $allPizzas[$x].", ";
            }
            echo "Preis:".$billingPrice."</p>";
            $ButtonText="Abgeliefert";
            echo "<input type='submit' name='$Customer[id]' value='$ButtonText'></form>";
        }


    }


        $this->generatePageFooter();
    }

    /**
     * Processes the data that comes via GET or POST i.e. CGI.
     * If this page is supposed to do something with submitted
     * data do it here.
     * If the page contains blocks, delegate processing of the
     * respective subsets of data to them.
     *
     * @return none
     */
    protected function processReceivedData()
    {
        parent::processReceivedData();
        // to do: call processReceivedData() for all members

        if(!is_null($_POST) and isset($_POST) and sizeof($_POST)==1){           //change delivery state if a submit button was clicked
            foreach ($_POST as $key=>$value){
                $this->_database->query("UPDATE ordered_articles SET status=status+1 WHERE f_order_id='$key'");
            }
            $_POST=null;

        }


    }

    /**
     * This main-function has the only purpose to create an instance
     * of the class and to get all the things going.
     * I.e. the operations of the class are called to produce
     * the output of the HTML-file.
     * The name "main" is no keyword for php. It is just used to
     * indicate that function as the central starting point.
     * To make it simpler this is a static function. That is you can simply
     * call it without first creating an instance of the class.
     *
     * @return none
     */
    public static function main()
    {
        try {
            $page = new Fahrer();
            $page->processReceivedData();
            $page->generateView();
        }
        catch (Exception $e) {
            header("Content-type: text/plain; charset=UTF-8");
            echo $e->getMessage();
        }
    }
}

// This call is starting the creation of the page.
// That is input is processed and output is created.
Fahrer::main();

