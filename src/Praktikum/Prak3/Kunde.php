<?php

require_once './Page.php';
class Kunde extends Page
{
    protected $allOrders =null;             //contains all orders
    // to do: declare reference variables for members
    // representing substructures/blocks

    /**
     * Instantiates members (to be defined above).
     * Calls the constructor of the parent i.e. page class.
     * So the database connection is established.
     *
     * @return none
     */
    protected function __construct()
    {
        parent::__construct();
        // to do: instantiate members representing substructures/blocks
    }

    /**
     * Cleans up what ever is needed.
     * Calls the destructor of the parent i.e. page class.
     * So the database connection is closed.
     *
     * @return none
     */
    public function __destruct()
    {
        parent::__destruct();
    }

    /**
     * Fetch all data that is necessary for later output.
     * Data is stored in an easily accessible way e.g. as associative array.
     *
     * @return none
     */
    protected function getViewData()
    {
        // to do: fetch data for this view from the database
        $sql="SELECT o.id, o.f_order_id, c.address, o.status, a.name, a.price FROM ordered_articles o JOIN article a ON a.id=o.f_article_id JOIN ordering c on c.id=o.f_order_id WHERE o.f_order_id='$_SESSION[id]'"; //create a join table with customer article and Orders
        if($this->_database->query($sql)){
            $this->allOrders=$this->_database->query($sql);
        }else{
            echo "Database access error".$this->_database->error;
        }

    }

    /**
     * First the necessary data is fetched and then the HTML is
     * assembled for output. i.e. the header is generated, the content
     * of the page ("view") is inserted and -if avaialable- the content of
     * all views contained is generated.
     * Finally the footer is added.
     *
     * @return none
     */
    protected function generateView()
    {
        $this->getViewData();
        $this->generatePageHeader("KundenSeite");
        // to do: call generateView() for all members
        // to do: output view of this page
        echo "
        <head>
        <meta charset=\"utf-8\"/>
        <meta name=\"keywords\" content=\"Essen, Pizza, Service\"> 
        <meta name=\"description\" content=\Pizza service Kunden info Seite\">
        <meta name=\"author\" content=\"Stefan &amp; Andreas\">
        <meta http-equiv=\"refresh\" content=\"60\">
        <title>Kunden Info Seite</title>
        </head>
        <body>
        <h1>Pizzaservice Binary</h1>
        <p>Bestellen Sie bei uns die beste Pizza im Landkreis!</p>
        <hr>
        <h2>Ausstehende lieferungen :</h2>";
        echo"<ul>";

        //create a list of all Orders
        foreach ($this->allOrders as $bes){
            $bes["f_order_id"] =htmlspecialchars(stripcslashes(trim($bes["f_order_id"])));
            $bes["address"] =htmlspecialchars(($bes["address"]));
            echo "<li> Bestell ID: ".$bes["id"]." Bestellung: ".$bes["name"]." Kundennummer: ". $bes["f_order_id"]." Lieferadresse: ".$bes["address"]."</li>";
        }
        echo"</ul>";
        $this->generatePageFooter();
    }

    /**
     * Processes the data that comes via GET or POST i.e. CGI.
     * If this page is supposed to do something with submitted
     * data do it here.
     * If the page contains blocks, delegate processing of the
     * respective subsets of data to them.
     *
     * @return none
     */
    protected function processReceivedData()
    {
        parent::processReceivedData();
        // to do: call processReceivedData() for all members
        session_start();
    }

    /**
     * This main-function has the only purpose to create an instance
     * of the class and to get all the things going.
     * I.e. the operations of the class are called to produce
     * the output of the HTML-file.
     * The name "main" is no keyword for php. It is just used to
     * indicate that function as the central starting point.
     * To make it simpler this is a static function. That is you can simply
     * call it without first creating an instance of the class.
     *
     * @return none
     */
    public static function main()
    {
        try {
            $page = new Kunde();
            $page->processReceivedData();
            $page->generateView();
        }
        catch (Exception $e) {
            header("Content-type: text/plain; charset=UTF-8");
            echo $e->getMessage();
        }
    }
}

// This call is starting the creation of the page.
// That is input is processed and output is created.
Kunde::main();

