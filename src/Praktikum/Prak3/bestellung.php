<?php

require_once './Page.php';
class bestellung extends Page
{
    protected $allArticles=null;            //Contains all Articles
    // to do: declare reference variables for members
    // representing substructures/blocks

    /**
     * Instantiates members (to be defined above).
     * Calls the constructor of the parent i.e. page class.
     * So the database connection is established.
     *
     * @return none
     */
    protected function __construct()
    {
        parent::__construct();
        // to do: instantiate members representing substructures/blocks
    }

    /**
     * Cleans up what ever is needed.
     * Calls the destructor of the parent i.e. page class.
     * So the database connection is closed.
     *
     * @return none
     */
    public function __destruct()
    {
        parent::__destruct();
    }

    /**
     * Fetch all data that is necessary for later output.
     * Data is stored in an easily accessible way e.g. as associative array.
     *
     * @return none
     */
    protected function getViewData()
    {
        // to do: fetch data for this view from the database
        $this->allArticles= array();
        // to do: fetch data for this view from the database
        $sql="SELECT * FROM article";
        $res=$this->_database->query($sql);
        if($res->num_rows>0){
            $cnt=0;
            while($index=$res->fetch_assoc()) {
                //echo "ID:" . $index["id"] . "Name" . $index["name"] . "Preis" . $index["price"];
                $this->allArticles[$cnt] = $index;
                $cnt++;
            }
        }else{
            echo "Database access error".$this->_database->error;
        }

    }

    /**
     * First the necessary data is fetched and then the HTML is
     * assembled for output. i.e. the header is generated, the content
     * of the page ("view") is inserted and -if avaialable- the content of
     * all views contained is generated.
     * Finally the footer is added.
     *
     * @return none
     */
    protected function generateView()
    {
        $this->getViewData();
        $this->generatePageHeader('Bestellung');
        // to do: call generateView() for all members
        // to do: output view of this page
        echo "
        <head>
        <meta charset=\"utf-8\"/>
        <meta name=\"keywords\" content=\"Essen, Pizza, Service\"> 
        <meta name=\"description\" content=\Pizza service bestell seite\">
        <meta name=\"author\" content=\"Stefan &amp; Andreas\">
        <meta http-equiv=\"refresh\" content=\"60\">
        <title>Bestellseite</title>
        </head>
        <body>
        <h1>Pizzaservice Binary</h1>
        <p>Bestellen Sie bei uns die beste Pizza im Landkreis!</p>
        <hr>
        <h2>Unsere Pizzen:</h2>";
        echo '<form action="'.htmlspecialchars("$_SERVER[PHP_SELF]").'" method="POST">';
        
        foreach($this->allArticles as $index) {             //Menue card Visualisation
            $tmpName=$index["name"]; $tmpPicture=$index["picture"]; $tmpPrice=$index["price"];
echo <<<EOT
            <h3>$tmpName</h3>
            <article><img src=$tmpPicture alt=$tmpName ='400'>
            <h4>$tmpPrice</h4></article>
EOT;

        }
        //shopping cart Visualisation
echo <<<EOT
        <select name="Warenkorb[]" size="6" multiple>
                    <option value=1 >Salami</option>
                    <option value=1 >Salami</option>
                    <option value=2>Vegetaria</option>
                    <option value=3>Spinat-Hühnchen</option>
                </select><br>
EOT;
        echo"KundenNummer: <input type='text' name='kundenId' value='' required>";
        echo "KundenAdresse:<input type='text' name='address' value='' required >";
        echo "<input type='submit' name='bestellen' value='Bestellung abschicken'>";
        $this->generatePageFooter();
    }

    /**
     * Processes the data that comes via GET or POST i.e. CGI.
     * If this page is supposed to do something with submitted
     * data do it here.
     * If the page contains blocks, delegate processing of the
     * respective subsets of data to them.
     *
     * @return none
     */
    protected function processReceivedData()
    {
        parent::processReceivedData();
        // to do: call processReceivedData() for all members
        session_start();

        if (!is_null($_POST) and isset($_POST) and sizeof($_POST) == 4 and $_POST["bestellen"] == "Bestellung abschicken"){

            $_SESSION["id"]=$_POST["kundenId"];

            var_dump($_POST["address"]);
            //$sql="SELECT id FROM ordering WHERE id= '$_POST[kundenId]'";
            $sql= sprintf("SELECT id FROM ordering WHERE id='%s'",$this->_database->real_escape_string($_POST["kundenId"]));
            $currentDateTime = date('Y-m-d H:i:s');      //generate timestamp
            //Insert new Customer/Orders in Database
            if($this->_database->query($sql)){
                //$sql="INSERT INTO ordering(id,address,timestamp) VALUE('$_POST[kundenId]','$_POST[address]', '$currentDateTime' )";
                $sql=sprintf("INSERT INTO ordering(id,address,timestamp) VALUE('%s','%s', '%s' )",$this->_database->real_escape_string($_POST["kundenId"]),$this->_database->real_escape_string($_POST["address"]),$currentDateTime);
                echo $sql;
                $this->_database->query($sql);

            }

            //Insert Shopping cart in Database
            $shoppingCart = $_POST["Warenkorb"];
        for($x=0; $x<count($shoppingCart); $x++) {
            $kundenid=$this->_database->real_escape_string($_POST["kundenId"]);
            $sql = "INSERT INTO ordered_articles(f_article_id,f_order_id, status) VALUE ('$shoppingCart[$x]','$kundenid','0') ";
            $this->_database->query($sql);
        }
        $_POST=null;
           header('Location: /Praktikum/Prak3/Kunde.php');
    }

    }


    /**
     * This main-function has the only purpose to create an instance
     * of the class and to get all the things going.
     * I.e. the operations of the class are called to produce
     * the output of the HTML-file.
     * The name "main" is no keyword for php. It is just used to
     * indicate that function as the central starting point.
     * To make it simpler this is a static function. That is you can simply
     * call it without first creating an instance of the class.
     *
     * @return none
     */
    public static function main()
    {
        try {
            $page = new bestellung();
            $page->processReceivedData();
            $page->generateView();
        }
        catch (Exception $e) {
            header("Content-type: text/plain; charset=UTF-8");
            echo $e->getMessage();
        }
    }
}

// This call is starting the creation of the page.
// That is input is processed and output is created.
bestellung::main();

