

function addToCard(PizzaName,PizzaID, Preis) {
    "use strict";
    var warenkorb=document.getElementById("Warenkorb");
    var option=document.createElement("option");
    var btn=document.getElementById("sbutton");

    option.text=PizzaName;
    option.value=PizzaID;
    option.dataset["price"]=parseFloat(Preis);

    warenkorb.add(option);
    calcPrice(parseFloat(Preis));
    btn.disabled=false;
}


function calcPrice(price) {
    "use strict";
    var total=document.getElementById("total");
    var totalInFloat=parseFloat(total.innerText);

    totalInFloat=totalInFloat+(price);
    total.innerText=totalInFloat.toFixed(2)+'€';
}


function selectAll(){
    "use strict";
    var card=document.getElementById("Warenkorb");

    for(var optOfCard of card){
        optOfCard.selected=true;
    }
}


function delArticle(removeAll) {
    "use strict";
    var card = document.getElementById("Warenkorb");
    var found=false;

        while(true) {
            found=false;
            for (var optOfCard of card) {
                if (removeAll==true || optOfCard.selected == true ) {
                    calcPrice(parseFloat(optOfCard.dataset["price"]) * (-1));
                    optOfCard.remove();
                    found=true;
                    break;
                }

            }
            if(found==false){
                break;
            }
        }

        if(card.length==0){
            var button=document.getElementById("sbutton");
            button.disabled=true;
        }

}

function setRadioButtonChecked(id,status) {
    "use strict";
    var Null=document.getElementById(id+"NULL");
    var Eins=document.getElementById(id+"Eins");
    var Zwei=document.getElementById(id+"Zwei");
    switch (status) {
        case 0: Null.checked=true;
        break;
        case 1: Eins.checked=true;
        break;
        case 2: Zwei.checked=true;
        break;
    }
}

