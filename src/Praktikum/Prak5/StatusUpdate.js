var request=new XMLHttpRequest();
function process(jsonString){
    var array=JSON.parse(jsonString);
    if(array.length >0) {
        for (var i = 0; i < array.length; i++) {
            var statusText =document.getElementById(array[i].id);
            switch (parseInt(array[i].status)) {
                case 0:
                    statusText.innerText = "Status: bestellt";
                    break;
                case 1:
                    statusText.innerText = "Status: backt";
                    break;
                case 2:
                    statusText.innerText = "Status: lieferfertig";
                    break;
                case 3:
                    statusText.innerText = "Status: wird ausgeliefert";
                    break;
                case 4:
                    statusText.innerText = "Status: geliefert"
            }
        }
    }else{
        document.write("Es wurde keine besetellte Pizza gefunden");
    }
}


function requestData() {
    request.open("GET","KundenStatus.php");
    request.onreadystatechange=processData;
    request.send(null);
}

function processData(){
    if(request.readyState==4){
        if(request.status==200){
            if(request.responseText!=null){
                process(request.responseText);
            }else{
                console.error("Dokument ist leer");
            }
        }else{
            console.error("Übertragung fehlgeschlagen");
        }
    }else{
        //übertragung läuft noch
    }
}
