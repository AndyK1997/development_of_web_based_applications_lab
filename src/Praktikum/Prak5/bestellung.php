<?php
require_once './Page.php';
class bestellung extends Page
{
    protected $_allArticles;            //Contains all Articles

    protected function __construct()
    {
        parent::__construct();
        // to do: instantiate members representing substructures/blocks
    }

    public function __destruct()
    {
        parent::__destruct();
    }
    /**
     *get Data from database
     */
    protected function getViewData()
    {
        // to do: fetch data for this view from the database
        $this->_allArticles= array();
        // to do: fetch data for this view from the database
        $sql="SELECT * FROM article"; // ArticleTable= id|name|picture|price
        $res=$this->_database->query($sql);
        if($res->num_rows>0){
            $ArrayIndex=0;
            //Write data in Array
            while($index=$res->fetch_assoc()) {

                $this->_allArticles[$ArrayIndex] = $index;
                $ArrayIndex++;
            }
        }else{
            echo "Database access error".$this->_database->error;
        }

    }
    protected function generateHtmlForm(){
        $sysAction=htmlspecialchars($_SERVER["PHP_SELF"]); //url geichert gegen cross scripting
        echo<<<EOT
        <section class="orderItems">
        <form name="CustomerInput" action="$sysAction" method="post" accept-charset="UTF-8">
        <p>Warenkorb</p>
        <select  id="Warenkorb" name="Warenkorb[]" multiple class="card" ></select><br>
        <input type="button" value="alle Löschen" onclick="delArticle(true)" class="button">
        <input type="button" value="Auswahl loeschen" onclick="delArticle(false)" class="button">
        <p class="total" id="total">0.00€</p>
        <label>Kunden Nr.:<input type="text" name="kundenId" value="" required></label>
        <label>Kunden Adresse:<input type="text" name="address" value="" required></label>
        <input id="sbutton" type="submit" name="bestellen" value="Bestellung abschicken" onclick="selectAll()" disabled class="button">
        </form>
        </section>
EOT;
    }
    protected function generateHtmlHeader(){
        echo<<<EOT
            <head>
            <meta charset="utf-8">
            <meta name="keywords" content="googleSuchwort1, googleSuchwort2, etc"> 
            <meta name="description" content="Beschreibung des webseiten Inhalts">
            <meta name="author" content="AutorName1 &amp; Andreas">
            <link rel='stylesheet' type='text/css' href='KLexample.css'><!--Einbindung der CSS Datei-->
            <script src='script.js'></script><!--Einbindung der JavaScript Datei-->
            <title>Bestellseite</title>
            </head>
EOT;
    }
    protected function generateHtmlBody(){
        echo<<<EOT
            <body>
            <div class="container">
            <header>
            <h1>Pizzaservice Binary</h1>
            <p>Bestellung sie bei uns die beste Pizza im Landkreis!</p>
            <hr>
            </header>
            <nav>
            <a class="navElement" href="bestellung.php">Bestellen</a>
            <a class="navElement" href="Pizzabaecker.php">Bäcker-Übersicht</a>
            <a class="navElement" href="Fahrer.php">Fahrer-Übersicht</a>
            </nav>
            <section class="speisekarte">
            <h2>Unsere Pizzen</h2>
EOT;
        foreach ($this->_allArticles as $Article) {
            $tmpId=$Article["id"]; $tmpName=$Article["name"]; $tmpPicture=$Article["picture"]; $tmpPrice=$Article["price"];

            echo<<<EOT
                <article class="pizza">
                <h3>$tmpName</h3>
                <img src="$tmpPicture" alt="" title="Pizza.$tmpName" width="250" height="250" onclick="addToCard('$tmpName',$tmpId,$tmpPrice)" >
                <h4 class="price">$tmpPrice €</h4>
                </article>
EOT;
        }
        echo<<<EOT
            </section>
EOT;
        $this->generateHtmlForm();
        echo<<<EOT
        <footer>
        <p>Pizzaservice Binary Tel:11 8 33, Schoefferstr 3, 64295 Darmstadt</p>
        </footer>
        </div>
        </body>
EOT;
    }
    /**
     * create Html Site
     */
    protected function generateView()
    {
        $this->getViewData();
        $this->generatePageHeader('Bestellung');
        $this->generateHtmlHeader();
        $this->generateHtmlBody();
        $this->generatePageFooter();
    }

    protected function processReceivedData()
    {
        parent::processReceivedData();
        session_start();

        if (!is_null($_POST)) {
            if (isset($_POST["kundenId"]) and isset($_POST["address"]) and isset($_POST["Warenkorb"])) {
                $_SESSION["id"] = $_POST["kundenId"];
                $shoppingCart = $_POST["Warenkorb"];
                $orderStatus=0;
                $kundenIdEscaped=$this->_database->real_escape_string($_POST["kundenId"]);

                $sql=sprintf("SELECT id FROM ordering WHERE='%s'",$kundenIdEscaped );

                if(!$this->_database->query($sql)) {
                    $currentDateTime = date('Y-m-d H:i:s');      //generate timestamp
                    $addressEscaped = $this->_database->real_escape_string($_POST["address"]);
                    $sql = sprintf("INSERT INTO ordering(id,address,timestamp) VALUE('%s','%s', '%s' )",
                        $kundenIdEscaped, $addressEscaped, $currentDateTime);
                    $this->_database->query($sql);
                }
                //Insert Shopping cart in Database
                for ($x = 0; $x < count($shoppingCart); $x++) {
                    $sql = "INSERT INTO ordered_articles(f_article_id,f_order_id, status) VALUE 
                        ('$shoppingCart[$x]','$kundenIdEscaped','$orderStatus') ";
                    $this->_database->query($sql);
                }
                $_POST = null;
                header('Location: /Praktikum/Prak5/Kunde.php');
            }
        }
    }

    public static function main()
    {
        try {
            $page = new bestellung();
            $page->processReceivedData();
            $page->generateView();
        }
        catch (Exception $e) {
            header("Content-type: text/plain; charset=UTF-8");
            echo $e->getMessage();
        }
    }
}

bestellung::main();

