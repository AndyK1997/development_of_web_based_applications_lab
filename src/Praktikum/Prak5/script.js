function addToCard(PizzaName,PizzaID, Preis) {
    "use strict";
    var Form=document.forms["CustomerInput"];
    var option=document.createElement("option");
    var warenkorb= Form.elements["Warenkorb"];
    var SubmitButton=Form.elements["sbutton"];
    option.text=PizzaName; //Schreibt den namen der Pizza in das select item
    option.value=PizzaID;  //Schreibt die Pizza ID in die Value des select items
    option.dataset["price"]=parseFloat(Preis);
    warenkorb.add(option); //fügt das item der Selectbox hinzu
    calcPrice(parseFloat(Preis));
    SubmitButton.disabled=false;
}
function calcPrice(price) {
    "use strict";
    var total = document.getElementById("total");
    var totalInFloat = parseFloat(total.innerText);
    totalInFloat = totalInFloat + (price);
    total.innerText = totalInFloat.toFixed(2) + '€'; //legt die anzahl der nachkommastellen fest
}
function selectAll(){
    "use strict";
    var card=document.getElementById("Warenkorb");
    for(var optOfCard of card){
        optOfCard.selected=true;
    }
}
function delArticle(removeAll) {
    "use strict";
    var card = document.getElementById("Warenkorb");
    var found=false;
        while(true) {
            found=false;
            for (var optOfCard of card) {
                if (removeAll==true || optOfCard.selected == true ) {
                    calcPrice(parseFloat(optOfCard.dataset["price"]) * (-1)); //übergibt einen negativen wert
                    optOfCard.remove();
                    found=true;
                    break;
                }
            }
            if(found==false){
                break;
            }
        }
        if(card.length==0){
            var button=document.getElementById("sbutton");
            button.disabled=true;
        }
}
function setRadioButtonChecked(id,status) {
    "use strict";
    var Null=document.getElementById(id+"NULL");
    var Eins=document.getElementById(id+"Eins");
    var Zwei=document.getElementById(id+"Zwei");
    switch (status) {
        case 0: Null.checked=true;
        break;
        case 1: Eins.checked=true;
        break;
        case 2: Zwei.checked=true;
        break;
    }
}

