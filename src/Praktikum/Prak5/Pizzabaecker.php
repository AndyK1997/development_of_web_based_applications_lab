<?php
require_once './Page.php';
class Pizzabaecker extends Page
{
    protected $BestellDaten =array();

    protected function __construct()
    {
        parent::__construct();
        // to do: instantiate members representing substructures/blocks
    }

    public function __destruct()
    {
        parent::__destruct();
    }

    protected function getViewData()
    {
        // to do: fetch data for this view from the database
        $sql="SELECT o.id, o.f_order_id, o.status, a.name ".
            "FROM ordered_articles o ".
            "JOIN article a ON a.id=o.f_article_id ".
            "WHERE o.status<3 order by o.id;";

        $res=$this->_database->query($sql);
        if($res->num_rows>0){
            $ArrayIndex=0;
            while($index=$res->fetch_assoc()){

                $this->BestellDaten[$ArrayIndex]=$index;
                $ArrayIndex++;
            }
        }else{
            echo "Database access error".$this->_database->error;
        }
    }
    protected function generateHtmlHeader(){
        echo<<<EOT
        <head>
        <meta charset="utf-8"/>
        <meta name="keywords" content="Essen, Pizza, Service"> 
        <meta name="description" content="Pizza service status seite">
        <meta name="author" content="Stefan &amp; Andreas">
        <meta http-equiv="refresh" content="5">
        <link rel='stylesheet' type='text/css' href='style.css'>
        <script src='script.js' ></script>
        <title>Pizzabaereckerseite</title>
        </head>
EOT;
    }
    protected function generateHtmlBody(){
        echo<<<EOT
        <body>
        <div class='container'>
        <header>
        <h1>Pizzaservice Binary</h1>
        <p>Bestellen Sie bei uns die beste Pizza im Landkreis!</p>
        <hr>
        </header>
        <section class='mainFrame'>
        <h2>Bestellungen:</h2>
EOT;
        if (sizeof($this->BestellDaten) != 0) {
            $formID="fomrm_id";
            $sysAction=htmlspecialchars($_SERVER["PHP_SELF"]);
            $sub="document.forms['".$formID."'].submit();";
            echo<<<EOT
            <ul><form id="$formID" action="$sysAction" method="POST" accept-charset="UTF-8">
EOT;
            foreach ($this->BestellDaten as $index) {
                $bestellId = $index["id"];
                $Pizza = $index["name"];
                $status=$index["status"];
                $N=$bestellId."NULL";
                $E=$bestellId."Eins";
                $Z=$bestellId."Zwei";

                echo <<<EOT
                
                <li><p>BestellNr: $bestellId Pizza: $Pizza </p>
                <label>Bestellt: <input id="$N" type="radio" name="$bestellId" value="0" onclick="$sub"></label>
                <label>Backt: <input id="$E" type="radio" name="$bestellId" value="1" onclick="$sub"></label>
                <label> gebacken: <input id="$Z" type="radio" name="$bestellId" value="2" onclick="$sub"></label></li>
                <script> setRadioButtonChecked('$bestellId',$status)</script>
EOT;
            }
            echo "</ul></form></div>";
        } else{
            echo "<p>Es gibt nichts zu tun</p></div>";
        }
        echo<<<EOT
        <footer>
        <p>Pizzaservice Binary Tel:11 8 33, Schoefferstr 3, 64295 Darmstadt</p>
        </footer>
        </div>
EOT;

    }

    protected function generateView()
    {
        $this->getViewData();
        $this->generatePageHeader("Pizzabaecker");
        $this->generateHtmlHeader();
        $this->generateHtmlBody();
        $this->generatePageFooter();
    }

    protected function processReceivedData()
    {
        parent::processReceivedData();

        if(isset($_POST)) {
            foreach ($_POST as $key => $value){
                $sql ="UPDATE ordered_articles SET status='$value' WHERE id=$key";
                $this->_database->query($sql);
            }
        }
    }

    public static function main()
    {
        try {
            $page = new Pizzabaecker();
            $page->processReceivedData();
            $page->generateView();
        }
        catch (Exception $e) {
            header("Content-type: text/plain; charset=UTF-8");
            echo $e->getMessage();
        }
    }
}
Pizzabaecker::main();
