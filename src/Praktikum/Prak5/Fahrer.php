<?php

require_once './Page.php';
class Fahrer extends Page
{
    protected $allOrders=null;       //contains all Orders
    protected $allDeliverables=null;      //contains all Customer/orderings

    protected function __construct()
    {
        parent::__construct();
        // to do: instantiate members representing substructures/blocks
    }
    public function __destruct()
    {
        parent::__destruct();
    }
    protected function getViewData()
    {
        // to do: fetch data for this view from the database
        $sql="SELECT o.id, o.f_article_id, o.f_order_id, o.status, a.name, a.price".
            " FROM ordered_articles o ".
            "JOIN article a ON a.id=o.f_article_id  ".
            "WHERE o.status in (2,3)".
            "Order by o.id"; //create a join table with article and orders

        $sql2="SELECT c.id, MAX(o.status) as m_Status ,MIN(o.status) as min_Status, c.address ".
            "FROM ordered_articles o ".
            "JOIN ordering c ON c.id=o.f_order_id ".
            "GROUP BY c.id, c.address ".
            "HAVING (MAX(o.status) <4 OR MIN(o.status) <4 ) AND MIN(o.status)>=2"; //all orders not completly delivered

        $this->allOrders=$this->_database->query($sql);
        $this->allDeliverables=$this->_database->query($sql2);

    }

    protected function generateHtmlHeader(){
        echo<<<EOT
         <head>
        <meta charset="utf-8">
        <meta name="keywords" content="Essen, Pizza, Service"> 
        <meta name="description" content="Pizza service Statusseite">
        <meta name="author" content="Stefan &amp; Andreas">
        <meta http-equiv="refresh" content="5">
        <script src='script.js'> </script>
        <link rel='stylesheet' type='text/css' href='style.css'>
        <title>Fahrer Seite</title>
        </head>
EOT;
    }
    protected function generateHtmlBody(){
        echo<<<EOT
        <body>
        <div class='container'>
        <header>
        <h1>Pizzaservice Binary</h1>
        <p>Bestellen Sie bei uns die beste Pizza im Landkreis!</p>
        <hr>
        </header>
        
        <section class='mainFrame'>
        <h2>Fertig zur Auslieferung:</h2>
EOT;
        foreach ($this->allDeliverables as $customer){
            $customerId=htmlspecialchars($customer["id"]);
            $address= htmlspecialchars($customer["address"]);
            $found=false;
            $Total=0;
            echo<<<EOT
            <article class="order">
            <p>Adresse: $address<br>&nbsp;Enthält:
EOT;
            foreach ($this->allOrders as $Pizza){
                if($Pizza["f_order_id"]==$customerId){
                    //ausgabe pizza
                    if($found==true){
                        echo", ";
                    }
                    echo $Pizza["name"];
                    $Total+=$Pizza["price"];
                    $found=true;
                }elseif ($found==true){

                    break;
                }

            }
            if($found==true) {
                echo "<br>&nbsp;Gesamt: ".$Total . "€";
            }else{
                echo "Keine Pizza gefunden";
            }
            echo "</p>";

            $sysAction=htmlspecialchars($_SERVER["PHP_SELF"]);
            $sub="document.forms['id".$customerId."'].submit();";
            $minStatus=$customer["min_Status"];

            if($minStatus==2){
                echo<<<EOT
                <form id="id$customerId" action="$sysAction" method="POST" accept-charset="UTF-8">
                <label>&nbsp;im Fahrzeug<input type="radio" name="$customerId" value="3" onclick="$sub" class="radio"></label>
                <label>&nbsp;Abgeliefert<input type="radio" name="$customerId" value="4" onclick="$sub" class="radio" disabled></label>
                <hr>
                </form>
EOT;
            }elseif($minStatus==3){
                echo<<<EOT
                <form id="id$customerId" action="$sysAction" method="POST" accept-charset="UTF-8">
                <label>&nbsp;im Fahrzeug<input type="radio" name="$customerId" value="3" onclick="$sub" class="radio" checked></label>
                <label>&nbsp;Abgeliefert<input type="radio" name="$customerId" value="4" onclick="$sub" class="radio"></label>
                <hr>
EOT;
            }
            echo "</article>";
        }
        echo<<<EOT
        </section>
        <footer>
        <p>Pizzaservice Binary Tel:11 8 33, Schoefferstr 3, 64295 Darmstadt</p>
        </footer>
        </div>
EOT;
    }

    protected function generateView()
    {
        $this->getViewData();
        $this->generatePageHeader('Fahrerseite');
        $this->generateHtmlHeader();
        $this->generateHtmlBody();
        $this->generatePageFooter();

    }

    protected function processReceivedData()
    {
        parent::processReceivedData();
        // to do: call processReceivedData() for all members

        if(!is_null($_POST) and isset($_POST) and sizeof($_POST)>=1){           //change delivery state if a submit button was clicked
            foreach ($_POST as $key=>$value){
                $this->_database->query("UPDATE ordered_articles SET status='$value' WHERE f_order_id='$key'");
            }
            $_POST=null;
        }
    }

    public static function main()
    {
        try {
            $page = new Fahrer();
            $page->processReceivedData();
            $page->generateView();
        }
        catch (Exception $e) {
            header("Content-type: text/plain; charset=UTF-8");
            echo $e->getMessage();
        }
    }
}
Fahrer::main();

