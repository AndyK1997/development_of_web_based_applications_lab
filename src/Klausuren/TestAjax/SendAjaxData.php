<?php	// UTF-8 marker äöüÄÖÜß€
session_start();
class SendAjaxData
{
    protected  $serializedData;
    protected  $data;
    protected function generateView()
    {
        $this->processReceivedData();
        header("refresh:1;url=http://localhost/Klausuren/TestAjax/SendAjaxData.php");
        header("Content-Type: application/json; charset=UTF-8");

        if($_SESSION["data"]!=null){
            $this->serializedData = json_encode($_SESSION["data"]); //erstellt json string aus den empfnagenen daten
            echo $this->serializedData;                             //echo json string
        }else {
            echo null; //wenn keine Daten empfangen wurden echo null
        }
    }

    protected function processReceivedData()
    {
        if(isset($_GET["name"]) or isset($_GET["address"])) {   //Überprüfen ob die werte auchg gesetzt wurden
            if($_GET["name"]!=null or $_GET["address"]!=null) { //so viele or !=null wie es Form Felder gib
                $this->data->name = $_GET["name"];              //werte ins array schreiben wie es felder gib
                $this->data->address = $_GET["address"];        //
                $_SESSION["data"] = $this->data;                //Werte in der Séssion speichern
            }
        }
    }

    public static function main()
    {
        try {
            $page = new SendAjaxData();
            $page->processReceivedData();
            $page->generateView();
        }
        catch (Exception $e) {
            header("Content-type: text/plain; charset=UTF-8");
            echo $e->getMessage();
        }
    }
}

SendAjaxData::main();
