<?php
class AJAXMainPHP{
    protected function generateHtmlHeader(){
        echo<<<EOT
        <!DOCTYPE html>
        <html lang="de">
        <head>
            <meta charset="UTF-8"><!--Codierungsstandart-->
            <meta name="author" content="Stefan &amp; Andreas"><!--Ersteller der Seite-->
            <meta http-equiv="refresh" content="60" /><!--refresh Zeit 1min-->
            <meta http-equiv="Cache-Control" content="no-cache, no-store, must-revalidate" >
            <meta http-equiv="Pragma" content="no-cache" >
            <meta http-equiv="Expires" content="0" >
           
            <script src='ajax.js'></script><!--Einbinden einer JS Datei-->
            <title>Tabbeschrifftung der Seite</title>
        </head>
EOT;
    }

    protected function generateHtmlBody(){
        echo<<<EOT
        <body>
        
        <header>
        <h1 id="Headline">Ueberschrift </h1>
        <h3 id="subHeadline">unterUeberschrift</h3>
        <hr>
        </header>
        
        <form name="myForm">
            <label id="nameLabel">TestInput<input type="text" name="name" value="" placeholder="zum senden mit ajax" oninput="requestData()"></label><br><br>
            <label id="addressLabel">GetTestInput<input type="text" name="address" value="" placeholder="data from Ajax"oninput="requestData()"></label>
        </form>
      
        </body>
        
        <script>
        window.onload=requestData()
        </script>
   
        <noscript>
            <p>Bitte Java Script aktivieren </p>
        </noscript>
EOT;
    }

    protected function generateHtmlfooter(){
        echo<<<EOT
        </html>
EOT;
    }

    protected function getViewData(){

    }

    protected function generateView(){
        $this->getViewData();
        $this->generateHtmlHeader();
        $this->generateHtmlBody();
        $this->generateHtmlfooter();
    }

    protected function processReceivedData(){
        session_start();
    }

    public static function main()
    {
        try {
            $page = new AJAXMainPHP();
            $page->processReceivedData();
            $page->generateView();
        }
        catch (Exception $e) {
            header("Content-type: text/plain; charset=UTF-8");
            echo $e->getMessage();
        }
    }
}
AJAXMainPHP::main();
?>
