<?php

class JavaScriptExample{

    protected $Dataset;
    protected function generateHtmlHeader(){
        echo<<<EOT
        <!DOCTYPE html>
        <html lang="de">
        <head>
            <meta charset="UTF-8"><!--Codierungsstandart-->
            <meta name="keywords" content="Essen, Pizza, Service"><!--Schlagwörter für Google-->
            <meta name="description" content="Pizza service Bestellseite"><!--Beschreibung der Seite-->
            <meta name="author" content="Stefan &amp; Andreas"><!--Ersteller der Seite-->
            <meta http-equiv="refresh" content="60" /><!--refresh Zeit 1min-->
            <meta http-equiv="Cache-Control" content="no-cache, no-store, must-revalidate" >
            <meta http-equiv="Pragma" content="no-cache" >
            <meta http-equiv="Expires" content="0" >
            <link rel='stylesheet' type='text/css' href='design.css'><!--Einbinden einer css Datei-->
            <script src='script.js'></script><!--Einbinden einer JS Datei-->
            <title>Tabbeschrifftung der Seite</title>
        </head>
        
EOT;

    }

    protected function generateHtmlBody(){
        echo<<<EOT
        <body>
        <header>
        <h1 id="Headline">Ueberschrift </h1>
        <h3 id="subHeadline">unterUeberschrift</h3>
        <hr>
        </header>
        <p id="Zahl1">200</p>
        <select name="ListBox[]" size="3" multiple><!--size=anzahl ohne scrollen, Selectbox mit Mehrfachauswahl-->
            <option value="PizzaSalami">Pizza Salami</option><!--Item mit Form Wert=Value -->
            <option selected>Pizza Margeritha</option><!--Vorausgewähltes Item-->
            <option data-Preis="12.80">Pizza Hawai</option><!--Item mit data Wert für Dom -->
        </select>
       <form action="https://echo.fbi.h-da.de/" method="POST" accept-charset="UTF-8" name="testForm"><!--Codierung und die Submit Adresse-->
        <label>Kleine TextBox:
            <input type="text" name="TextFeldKlein" value="" placeholder="kleines Textfeld" required><!--EingabeFeld-->
        </label><br>
        </form>
      
        </body>
        <script>
            setHeadlines();
            main();
            Dom();
        </script>
        
        <noscript>
            <p>Bitte Java Script aktivieren </p>
        </noscript>
EOT;

    }

    protected function generateHtmlfooter(){
        echo<<<EOT
        </htm>
EOT;

    }


    protected function getViewData(){

    }
    protected function generateView(){
        $this->getViewData();
        $this->generateHtmlHeader();
        $this->generateHtmlBody();
        $this->generateHtmlfooter();
    }

    protected function processReceivedData(){

    }

    public static function main()
    {
        try {
            $page = new JavaScriptExample();
            $page->processReceivedData();
            $page->generateView();
        }
        catch (Exception $e) {
            header("Content-type: text/plain; charset=UTF-8");
            echo $e->getMessage();
        }
    }
}
JavaScriptExample::main();
?>
