<?php
/**
 * String concatinierung
 */
$x="Hallo";
$y="Heute ist ein Schöner Tag";
echo $x."Welt".$y;
/**---------------------------------------------------------------------------------------------------------------------
 * Arithmetrische Operationen
 */
$ERG=8+6; $ERG=8-6; //Addition ,Subtraktion
$ERG=8*6; $ERG=8/6;//Multiplikation, Divission
$ERG=8%6; //Modulo =2
$ERG=8**6; //Exponent 8^6;
/**---------------------------------------------------------------------------------------------------------------------
 * IF Statements
 */
if($ERG<100){
    //Do something
}elseif ($ERG==0){
    //Do something
}else{

}
/**---------------------------------------------------------------------------------------------------------------------
 * Switch Statement
 */
switch ($ERG){
    case 3: echo"Zahl ist 3"; break;
    case 8**6: echo"Zahl wurde exponenziert";break;
    case $ERG:{
        //großer case;
        break;
    }
}
/**---------------------------------------------------------------------------------------------------------------------
 * Schleifen
 */
while (true){

}
do{

}while(true);
$Array=array("Name","Wohnort");
for($x=0; $x<count($Array); $x++){

}
foreach ($Array as $key=>$value){
    //loop durch assoziatives key =50 =>value= Hallo
}
/**---------------------------------------------------------------------------------------------------------------------
 *Funktion Muster
 */
function Hello($text){
    echo $text;

    return true; //Typ muss nicht in der Funktion angegeben werden
}
Hello("Hallo World"); //-->Funktions Aufruf mit & für call by Reference
/**---------------------------------------------------------------------------------------------------------------------
 * Ausgaben auf der Website
 */
$Array["x"]=5;
$x=$Array["x"];
echo<<<EOT
    <!--Herodoc Notation  Html kann direct ausgeführt werden--> 
    <!--Array Zugriffe müssen vor Herdoc gemacht werden-->
    $x      <!--das geht in Herdoc-->
EOT;
/**---------------------------------------------------------------------------------------------------------------------
 * FormularAuswertung Muster
 */
if($_SERVER["REQUEST_METHOD"]=="POST"){
    $Params=$_POST;
    if(isset($Params["ElementName"]) and isset($Params["ElementName2"])){
        //Auf elemente Zugreifen
    }
}
/**---------------------------------------------------------------------------------------------------------------------
 * Ausnahme Behandlung
 */
try{
    if(sizeof($Array)==0){
        throw new Exception("Meine Fehlermeldung");//Muss nicht verwendet werden try catch reicht
    }
    $erg=$Array[0]/10; //->wird fehlschlagen wenn in 0 eine 0 steht
}catch (Exception $e){
    echo $e->getMessage();
}

/**---------------------------------------------------------------------------------------------------------------------
 * MySQLi verbinden (Datenbank)
 */
try {

    $Database = new MySQLi("localhost", "admin", "password", "postgres"); //Datenbank conect
    $Database->set_charset("utf8"); //Codierung setzten
    if(mysqli_connect_errno()){
        throw new Exception("Connect failed:".mysqli_connect_error());
    }
    if(!$Database->set_charset("utf8")){
        throw new Exception("Chrset failed".$Database->error);
    }
    $sql = "Select * from Person Where Name='andreas'"; //SQL Befehl
    $Recordset = $Database->query($sql);                //SQL ausführen
    if(!$Recordset) {
    throw new Exception("Query failed".$Database->error);
    }
    $cnt = 0;
    while ($index = $Recordset->fetch_assoc()) { //Recordset auswerten
        $Array[$cnt] = $index;
        $cnt++;
    }

    $Recordset->free();                 //Rercordset speichere freigeben
}catch(Exception $e){
    echo $e->getMessage();
}
$Database->close();                     //Datenbank verbindung lösen
/**---------------------------------------------------------------------------------------------------------------------
 * Sicherheit
 */
$Wert=$Database->real_escape_string($Wert); //Escaped Benutzer eingaben verhindert sql injection
echo htmlspecialchars($Wert); //Verhindert das HTML tags ausgeführt werden nur bei der Ausgabe verwenden

$sysAction=htmlspecialchars($_SERVER["PHP_SELF"]); //Gleiche PHP seite verhindert cross scripting in der url
echo<<<EOT
        <form action="$sysAction" method="post"></form> <!--Einbettung der Url absicherung-->
EOT;

/**---------------------------------------------------------------------------------------------------------------------
 * Session ohne PHP Session und cookies
 */
$SessionID=md5(uniqid(mt_rand()));  //generierung einer session id hier könnte auich die Kunden ID stehen bsp. Praktikum
echo<<<EOT
        <input type="hidden" name="SessionID" value="$SessionID"> <!--Session ID über ein Formular schicken-->
        <a href="HTMLexample.html?SessionID=$SessionID" >Test</a> <!--Session ID ohne Formular schicken-->
EOT;
/**---------------------------------------------------------------------------------------------------------------------
 * Session mit PHP Session
 */
session_start(); //ganz am anfang der website (processs recived data)
$SID=session_id(); //liefert die aktuelle session id
$_SESSION["myID"]=50; //diese Array ist  jetzt auf allen seiten mit der gleichen session id verfügbar
$_SESSION=array(); //löscht das Session array
session_destroy(); //beendet die session zb. beiu einem logout
/**
 * 153 ggf ergänzen
 */
?>