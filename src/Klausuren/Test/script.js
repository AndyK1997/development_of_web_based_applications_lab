/**
 * Funktion zum setzten des überschrift Textes
 */
function setHeadlines() {
    "use strict";
    let strinConcat ="today is a nice day";
    let Headline =document.getElementById("Headline");
    let subHeadline=document.getElementById("subHeadline");
    Headline.innerText="Hello World";
    subHeadline.innerText="Hello World"+strinConcat;
}
/**---------------------------------------------------------------------------------------------------------------------
 * let or var wann benutze ich was
 */
function example(){
    var Vector1=new Array();  //ohne Längenangabe
    let Vector2=new Array(27); //let mi tlängegangabe
    let Vector3 = new Array("abc","55");
    let string1="Hallo"
    var string2=document.getElementById("Zahl");
}
/**---------------------------------------------------------------------------------------------------------------------
 * Umgang mit arrays und Vectzoren
 */
function arrayHandler() {
    //Zugriff
    const ArraySize =5;
    let Vector=new Array(ArraySize);
    let Element=Vector[4];
    Vector[0]="text";
    let AnzahlElemente=Vector.length;
    for(var ElementofVector of Vector){
        //loop durch den Vector/Array
    }
}
/**---------------------------------------------------------------------------------------------------------------------
 * Vordefinierte Funktionen
 */
function vordefinierteFunktionen(option) {
    var Zahl =document.getElementById("Zahl1").innerText;
    switch(option){
        case 0:alert("HalloWelt"); //Message Box mit texT= Hallo Welt
        break;
        case 1: isFinite(Zahl);    // is numeric
        break;
        case 2: isNaN(Zahl);       //is not numeric
        break;
        case 3: parseFloat(Zahl);  //Zahl in Float
        break;
        case 4: parseInt(Zahl);    //Zahl in Int
        break;
        case 5: Zahl=Number(Zahl); //Wert in Zahl umwandel
        break;
        case 6: Zahl=String(Zahl); //Wert in String umwandeln
        break;
        case 7: Zahl.toFixed(2); //Zahl als string mit fester anzahl an Nachkomma Stellen
        break;
    }
}
/**---------------------------------------------------------------------------------------------------------------------
 * Klassen in Java Script
 */
class myClass{

    constructor() {
        this.Headline ="Hello from Class";
    }
    getHeadline(){
        return this.Headline;
    }
}
/**
 * main zum aufrufen der Klasse
 */
function main() {
    let newClass =new myClass();
    var Headline=document.getElementById("Headline");
    Headline.innerText=newClass.getHeadline();
}
/**---------------------------------------------------------------------------------------------------------------------
 *Ausnahmebahandlung
 */
function Ausnahhmebehandlung(){
    try{
        var irgendwas=null;
        if(irgendwas==null){
            throw "Fehler ist null";
        }
    }catch (e) {
        alert(e);
    }
}
/**---------------------------------------------------------------------------------------------------------------------
 * Zeitsteruerung
 */
function Zeitsewuerung() {
    window.setTimeout(Ausnahhmebehandlung, 5000); //verzögerung um 5 Sec
    let ID = window.setInterval(Ausnahhmebehandlung, 5000); //führt die Funktion alle 5 sec aus
    window.clearInterval(ID); //beendet die ausführung
}

/**---------------------------------------------------------------------------------------------------------------------
 * DOM steruerungs Elemente
 */
function Dom(){
    var Element;
    var Formular;
    Element=document.getElementById("123");     //Jedes Element braucht eine ID
    Element=document.getElementsByTagName("h1")[0]; //gibt mir das eerste Element der website mit dem TAg h1
    Element=document.getElementsByName("abc")[0]; // gibt mir das erste element mit dem Namen ->Radiobutton
    Formular=document.forms["testForm"];                       //zugriff auf das formular kann per id oder name sein
    Formular.elements["TextFeldKlein"].value="Hallo Welt";      //Zugriff auf ein Element im Formular (id oder name)
}

